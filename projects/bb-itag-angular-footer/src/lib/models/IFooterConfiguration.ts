import { ThemePalette } from '@angular/material/core';

export interface IFooterConfiguration {
  appVersion?: string;
  onlineThemePalette?: ThemePalette;
  offlineThemePalette?: ThemePalette;
  onlineFaIconDefinition?: any;
  offlineFaIconDefinition?: any;
  onlineText?: string;
  offlineText?: string;
  showPointer?: boolean;
  versionRouterLink?: string;
  versionRouterLinkClass?: string;
}
