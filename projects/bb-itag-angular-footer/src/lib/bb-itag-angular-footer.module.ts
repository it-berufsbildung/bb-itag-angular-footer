import { NgModule } from '@angular/core';
import { BbItagAngularFooterComponent } from './bb-itag-angular-footer.component';
import { MatToolbarModule } from '@angular/material';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [BbItagAngularFooterComponent],
  imports: [
    MatToolbarModule,
    FontAwesomeModule,
    CommonModule,
    RouterModule,
  ],
  exports: [BbItagAngularFooterComponent]
})
export class BbItagAngularFooterModule { }
