# bb-itag-angular-footer library

This is an Angular-footer library, which gives you a ready-made footer. It is possible to customize the footer with attributes. 
The footer is also able to adapt with a connection service depending on whether you are online or offline
This ReadMe explains how to integrate the footer and how to configure it.

### Dependencies

The footer requires Angular Material, FlexLayout, FontAwesome and NgConnectionService.

These can all be integrated via the terminal:
* Angular Material: 
```
ng add @angular/material
```
* FontAwesome: 
```
npm i -s @fortawesome/free-solid-svg-icons @fortawesome/angular-fontawesome@0.5.x
```
* FlexLayout:
```
npm i -s @angular/flex-layout @angular/cdk
```
* Connection Service:
```
npm i -s ng-connection-service
```

### Installation

install the library via NPM:
```text
npm i -s bb-itag-angular-footer
```

shortcut with all dependencies together:
```
npm i -s @fortawesome/free-solid-svg-icons @fortawesome/angular-fontawesome@0.5.x @angular/flex-layout @angular/cdk bb-itag-angular-footer ng-connection-service
```

### Using the library in the application

To use the library in your application, add add to the app.module ts the following import: 
``` typescript
import { BbItagAngularFooterModule } from 'bb-itag-angular-footer'

// add the footer module in the module imports
@NgModule({
  imports:
    BbItagAngularFooterModule
})

```

add the footer to your application. We recommend to create your own mainApp component but you can use it direct in the app-component files.

###### app.component.html 
```html
<div>
  <lib-bb-itag-angular-footer></lib-bb-itag-angular-footer>
  <router-outlet ></router-outlet>
</div>
```

This will show you a simple footer on the bottom with the current connection state and a default font awesome icon on the right site.

![footer_default]( https://gitlab.com/bb-itag/bb-itag-angular-footer/raw/master/projects/bb-itag-angular-footer/src/images/footer-online-default.PNG "Footer Default")

#### Footer Configuration

You can configure the footer so it allows you to only display the wanted elements.

First of all, we need to declare in the typescript file of our component which attributes we want wo use to configure the footer.
In the example below are all of the possible attributes listed.

###### app.component.ts

```typescript
  // the application version which gets displayed at the bottom left (default: null)
  public appVersion: string = 'Version: 1.0';
  // boolean if the cursor should be an pointer or an arrow. Needs to be true to use routerLink and click event (default: true)
  public showPointer: boolean = true;
  // the material color settings warn, accent or primary (default 'primary') of the footer when it's online
  public onlineThemePalette: ThemePalette = 'primary';
  // the material color settings warn, accent or primary (default 'warn') of the footer when it's offline
  public offlineThemePalette: ThemePalette = 'warn';
  // the font awesome icon that gets displayed when you're online (default: 'faNetworkWired')
  public onlineFaIconDefinition: any = faAddressBook;
  // the font awesome icon that gets displayed when you're offline (default: 'faExclamationTriangle')
  public offlineFaIconDefinition: any = faTable;
  // the text that gets displayed at the bottom right when you're online (default: 'ONLINE')
  public onlineText: string = 'connected';
  // the text that gets displayed at the bottom right when you're offline (default: 'OFFLINE')
  public offlineText: string = 'disconnected';
  // the router link on the version / text on the left. (default /)
  public versionRouterLink: string = null;
  // what global css class we have to use to show the active menu (default: 'defaultVersion' this is an internal class)
  public versionRouterLinkClass: string = 'footerActiveRouterLink';
```

Then we refer in app.component.html to the config settings. All parameters have a default value you only need to pass the attributes which you have declared in the typescript file.

###### app.component.html

``` html
  <lib-bb-itag-angular-footer [appVersion]="appVersion"
                              [showPointer]="showPointer"
                              [onlineThemePalette]="onlineThemePalette"
                              [offlineThemePalette]="offlineThemePalette"
                              [onlineFaIconDefinition]="onlineFaIconDefinition"
                              [offlineFaIconDefinition]="offlineFaIconDefinition"
                              [onlineText]="onlineText"
                              [offlineText]="offlineText"
                              [versionRouterLink]="versionRouterLink"
                              [versionRouterLinkClass]="versionRouterLinkClass"
                              (appVersionClicked)="onFooterAppVersionClicked($event)"
                              [online]="isOnline$ | async">
  </lib-bb-itag-angular-footer>
```

Now the footer should be displayed with the contents you've set and look like this:

![footer with attributes set](https://gitlab.com/bb-itag/bb-itag-angular-footer/raw/master/projects/bb-itag-angular-footer/src/images/footer-online-attributes.png "footer with attributes set")


### explanation of the attributes

#### appVersion

Display the application version on the left side of the footer.

###### datatype

``` typescript
string
```

###### default 
``` typescript
null
```

You can get the application version from the package.json if you want to like this:
```typescript
// @ts-ignore
import * as pjson from '../../package.json';

  public appVersion: string = `Version: ${pjson.version}`;
```

 
#### showPointer

When you hover over the application version you can decide if you want the cursor as pointer or as default.
If it's false then you're no able to use features like routerLink and the version click event on the version label.

###### datatype
```typescript 
boolean
```

###### default 
```typescript
true
```

#### onlineThemePalette

The onlineThemePalette that you can choose between `primary, accent, warn`. This defines the color of the footer when it's online

###### datatype

```typescript
import { ThemePalette } from '@angular/material/core';
```
###### default 

``` typescript
'primary'
```

#### offlineThemePalette

The offlineThemePalette that you can choose between `primary, accent, warn`.  This defines the color of the footer when it's offline

###### datatype

```typescript
import { ThemePalette } from '@angular/material/core';
```
###### default 

``` typescript
'warn'
```

#### onlineFaIconDefinition

With this attribute you can declare the font awesome icon that gets displayed when you're online. You just need to get the corresponding import from font awesome.

###### datatype

``` typescript
any
```

###### default

```typescript
faNetworkWired
```

#### offlineFaIconDefinition

With this attribute you can declare the font awesome icon that gets displayed when you're offline. You just need to get the corresponding import from font awesome.

###### datatype

``` typescript
any
```

###### default

```typescript
faExclamationTriangle
```

#### onlineText

The text that get displayed on right side when you online.

###### datatype

``` typescript
string
```

###### default

```typescript
'ONLINE'
```
 
#### offlineText

The text that get displayed on right side when you offline.

###### datatype

``` typescript
string
```

###### default

```typescript
'OFFLINE'
```
 
#### versionRouterLink

With the versionRouterLink ou can set an route where you get redirected to if you click on the application version.

###### datatype

```typescript
string
```

###### default

``` typescript
null
```

#### versionRouterLinkClass 

With versionRouterLinkClass you're able to set an css class, which is used when the routerLink is active. 
1. Create a class in your theme.scss or styles.scss just so it is global.
Example: 

```css
.activeVersionRouterLink { color: green; }
```
2. Set the class in your typescript file: 
```typescript
activeVersionRouterLink
```

###### datatype 

``` typescript
string
```

###### default 
Internal css class
``` typescript
footerActiveRouterLink 
```


#### Version Click Event

The footer libary has a ready-made click event that you can use and create a function for it.

Add to the template an click event called appVersionClicked:
```html
<lib-bb-itag-angular-footer (appVersionClicked)="onFooterClicked($event)"></lib-bb-itag-angular-footer>`
``` 
Then create in the typescript file of the component a method:

Example: 
```typescript
 onFooterClicked(appVersion: string) {
    // code
    alert(appVersion);
  }
```

#### connection-service

The connection service allows you that the footer adapts to the corresponding connection state. 
Withe the following changes: isOnline is an observable which you can subscribe to easy via the async pipe. The default value for the footer is true.

Online:
![footer online](https://gitlab.com/bb-itag/bb-itag-angular-footer/raw/master/projects/bb-itag-angular-footer/src/images/footer-online-attributes.png "footer online")

Offline:
![footer offline](https://gitlab.com/bb-itag/bb-itag-angular-footer/raw/master/projects/bb-itag-angular-footer/src/images/footer-offline-image.png "footer offline")


###### component.html

``` typescript
 <lib-bb-itag-angular-footer [online]="isOnline$ | async"></lib-bb-itag-angular-footer>
```
###### component.ts

```typescript
  public isOnline$: Observable<boolean>;

  constructor(
    connectionService: ConnectionService,
  ) {
    // this.isOnline$ = of(true);
    this.isOnline$ = connectionService.monitor();
  }
```

#### IFooterConfiguration

contains all possible settings for internal use only but you may can see all possible options here:

```typescript
export interface IFooterConfiguration {
  // the application version which gets displayed at the bottom left (default: null)
  appVersion: string;
  // the material color settings warn, accent or primary (default 'primary') of the footer when it's online
  onlineThemePalette: ThemePalette;
  // the material color settings warn, accent or primary (default 'warn') of the footer when it's offline
  offlineThemePalette: ThemePalette;
  // the font awesome icon that gets displayed when you're online (default: 'faNetworkWired')
  onlineFaIconDefinition: any;
  // the font awesome icon that gets displayed when you're offline (default: 'faExclamationTriangle')
  offlineFaIconDefinition: any;
  // the text that gets displayed at the bottom right when you're online (default: 'ONLINE')
  onlineText: string;
  // the text that gets displayed at the bottom right when you're offline (default: 'OFFLINE')
  offlineText: string;
  // boolean if the cursor should be an pointer or an arrow. Needs to be true to use routerLink and click event (default: true)
  showPointer: boolean;
  // the router link on the version / text on the left. (default /)
  versionRouterLink: string;
  // what global css class we have to use to show the active menu (default: 'defaultVersion' this is an internal class)
  versionRouterLinkClass: string;
}
```
