/* tslint:disable:no-inferrable-types */
import { Component } from '@angular/core';
import { faAddressBook, faTable } from '@fortawesome/free-solid-svg-icons';
import { IFooterConfiguration } from '../../projects/bb-itag-angular-footer/src/lib/models/IFooterConfiguration';
import { ConnectionService } from 'ng-connection-service';
import { Observable, of } from 'rxjs';
import { ThemePalette } from '@angular/material';
// @ts-ignore
import * as pjson from '../../package.json';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  public isOnline$: Observable<boolean>;

  constructor(
    connectionService: ConnectionService,
  ) {
    this.isOnline$ = of(true);
    // this.isOnline$ = connectionService.monitor();
  }

  // the application version which gets displayed at the bottom left (default: null)
  public appVersion: string = 'Version: 1.0';
  // boolean if the cursor should be an pointer or an arrow. Needs to be true to use routerLink and click event (default: true)
  public showPointer: boolean = true;
  // the material color settings warn, accent or primary (default 'primary') of the footer when it's online
  public onlineThemePalette: ThemePalette = 'primary';
  // the material color settings warn, accent or primary (default 'warn') of the footer when it's offline
  public offlineThemePalette: ThemePalette = 'warn';
  // the font awesome icon that gets displayed when you're online (default: 'faNetworkWired')
  public onlineFaIconDefinition: any = faAddressBook;
  // the font awesome icon that gets displayed when you're offline (default: 'faExclamationTriangle')
  public offlineFaIconDefinition: any = faTable;
  // the text that gets displayed at the bottom right when you're online (default: 'ONLINE')
  public onlineText: string = 'connected';
  // the text that gets displayed at the bottom right when you're offline (default: 'OFFLINE')
  public offlineText: string = 'disconnected';
  // the router link on the version / text on the left. (default /)
  public versionRouterLink: string = null;
  // what global css class we have to use to show the active menu (default: 'defaultVersion' this is an internal class)
  public versionRouterLinkClass: string = 'footerActiveRouterLink';

  onFooterAppVersionClicked(appVersion: string) {
    alert(appVersion);
  }

}
